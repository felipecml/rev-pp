/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.rev.pp.entidade;

import ifpb.rev.pp.beans.Pessoa;
import ifpb.rev.pp.dao.Dao;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;
/**
 *
 * @author Luciana
 */
public class DaoJPAMock {
    
    @Mock
    private Dao dao;
    
    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void testaSalvar(){
        
        Pessoa p = new Pessoa("Bel", "111111");
        
        when (dao.salvar(p)).thenReturn(Boolean.TRUE);
        assertTrue(dao.salvar(p));
        
    }
    
    @Test
    public void testaSalvarParamNull(){
    
        Pessoa p = new Pessoa("Bel", null);
        
        when (dao.salvar(p)).thenReturn(Boolean.FALSE);
        assertFalse(dao.salvar(p));
        
    }
    
    @Test
    public void testaPesquisar(){
    
        Pessoa p = new Pessoa("Bel", "111111");
        
        when(dao.pesquisar(1)).thenReturn(p);
        Pessoa p2 = (Pessoa)dao.pesquisar(1);
        
        assertNotNull("teste objeto nulo: ", p2);
        assertSame("teste se encontrou: ", p2, p);
        
    }
    
    @Test
    public void testeAtualizar(){
    
        Pessoa p = new Pessoa("Bel", "111111");
        
        when(dao.pesquisar(1)).thenReturn(p);
        Pessoa p2 = (Pessoa)dao.pesquisar(1);
        
        p2.setCpf("2222222");
        
        when(dao.atualizar(p2)).thenReturn(Boolean.TRUE);
        
        assertTrue("teste atualizar: ", dao.atualizar(p2));
        
    }
    
}
