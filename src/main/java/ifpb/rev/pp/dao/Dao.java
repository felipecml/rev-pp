/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ifpb.rev.pp.dao;

/**
 *
 * @author Luciana
 */
public interface Dao {

    public boolean salvar(Object obj);
    public boolean  atualizar(Object obj);
    public Object pesquisar (int id);
    
    
}
