/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.rev.pp.dao;

import ifpb.rev.pp.beans.Pessoa;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Luciana
 */
public class DaoJPA implements Dao {

    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("persistence");
    private EntityManager entity = factory.createEntityManager();

    @Override
    public boolean salvar(Object obj) {
        entity.getTransaction().begin();
        entity.persist(obj);
        entity.getTransaction().commit();
        
        return true;

    }

    @Override
    public boolean atualizar(Object obj) {
        entity.getTransaction().begin();
        entity.merge(obj);
        entity.getTransaction().commit();
        
       return true;
    }

    public Object pesquisar(int id) {
        return entity.find(Pessoa.class, id);
    }

    

}
