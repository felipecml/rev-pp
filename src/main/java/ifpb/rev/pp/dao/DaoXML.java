/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.rev.pp.dao;

import com.thoughtworks.xstream.XStream;
import ifpb.rev.pp.beans.Pessoa;
import ifpb.rev.pp.util.CreateDiretorio;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Felipe
 */
public class DaoXML implements Dao {

    private XStream xstream;
    private FileOutputStream fos;
    private File file;
    private Pessoa p;
    private String xml;
    private Object obj;

    public DaoXML() {
        this.xstream = new XStream();
        CreateDiretorio.criarDiretorio();
    }

    @Override
    public boolean salvar(Object obj) {

        p = (Pessoa) obj;
        file = new File(CreateDiretorio.CAMINHO_ARQUIVO + p.getId() + ".txt");

        if (file.exists()) {
            return false;
        }

        try {

            xml = xstream.toXML(p);
            fos = new FileOutputStream(file);
            fos.write(xml.getBytes());
            fos.close();

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }

        return true;

    }

    @Override
    public boolean atualizar(Object obj) {

        p = (Pessoa) obj;
        file = new File(CreateDiretorio.CAMINHO_ARQUIVO + p.getId() + ".txt");

        if (!file.exists()) {
            return false;
        }

        try {

            xml = xstream.toXML(p);
            fos = new FileOutputStream(file);
            fos.write(xml.getBytes());
            fos.close();

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public Object pesquisar(int id) {

        file = new File(CreateDiretorio.CAMINHO_ARQUIVO + id + ".txt");

        if (!file.exists()) {
            return null;
        }
        
        obj = xstream.fromXML(file);
        return obj;
        
    }
    
    
    
}
