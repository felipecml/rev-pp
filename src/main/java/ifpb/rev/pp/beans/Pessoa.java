/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ifpb.rev.pp.beans;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

/**
 *
 * @author Felipe
 */
@Entity
@TableGenerator(name = "pessoa_seq", allocationSize = 1, initialValue = 1)
public class Pessoa implements Serializable{
    
    @Id
    @GeneratedValue(generator = "pessoa_seq", strategy = GenerationType.TABLE)
    private int id;
    private String nome;
    private String cpf;

    public Pessoa() {
    }

    public Pessoa(int id) {
        this.id = id;
    }

    public Pessoa(String nome, String cpf) {
        this.nome = nome;
        this.cpf = cpf;
    }

    public Pessoa(int id, String nome, String cpf) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
}
